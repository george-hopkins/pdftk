## [3.0.3] - 2019-01-15

### Fixed
 - Bug that corrupts images in PDF files with (de)compress option
 - Crash with incomplete records

## [3.0.2] - 2018-12-05

### Fixed
 - Issue with rotation not being applied.
 - Do not require owner password when user password is given.

## [3.0.1] - 2018-09-30

### Fixed
 - Issue reading one file three or more times.

## [3.0.0] - 2018-09-04

### Added
 - Translation of pdftk into Java.
